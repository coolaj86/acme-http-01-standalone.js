'use strict';

var defaults = {};
var _memdb = {};

module.exports.create = function(config) {
	var memdb = config.cache || _memdb;

	return {
		init: function(opts) {
			//request = opts.request;
			return Promise.resolve(null);
		},

		set: function(data) {
			return Promise.resolve().then(function() {
				// console.log('Add Key Auth URL', data);

				var ch = data.challenge;
				var key = ch.identifier.value + '#' + ch.token;
				memdb[key] = ch.keyAuthorization;

				return null;
			});
		},

		get: function(data) {
			return Promise.resolve().then(function() {
				// console.log('List Key Auth URL', data);

				var ch = data.challenge;
				var key = ch.identifier.value + '#' + ch.token;

				if (memdb[key]) {
					return { keyAuthorization: memdb[key] };
				}

				return null;
			});
		},

		remove: function(data) {
			return Promise.resolve().then(function() {
				// console.log('Remove Key Auth URL', data);

				var ch = data.challenge;
				var key = ch.identifier.value + '#' + ch.token;

				delete memdb[key];
				return null;
			});
		}
	};
};
